package service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityCreator {

    public static EntityManagerFactory factory = Persistence.createEntityManagerFactory("learnhibernate");
    public static EntityManager entityManager = factory.createEntityManager();
}
