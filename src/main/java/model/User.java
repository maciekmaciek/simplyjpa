package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class User {

    @Id
    private int id;

    @Column(name = "firstName", nullable = false)
    private String name;

//    @Column(unique = true)
    @Transient
    // pole pesel ma nie byc mapowane do bazy danych ( przez użycie Transient )
    private String pesel;

    public User() {}

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
